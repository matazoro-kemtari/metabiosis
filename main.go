package main

import (
	"archive/zip"
	"bufio"
	"bytes"
	"crypto/md5"
	"encoding/csv"
	"encoding/hex"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/nfnt/resize"
)

func main() {

	// ファイルを取得
	textFiles := dirwalk("F:\\koban\\20210617")

	var entities [][]string
	for _, file := range textFiles {
		entities = append(entities, readTsvFile(file)...)
	}

	// 非同期実行 チャネル無し
	go writeXlsxFile(entities)

	numCPU := runtime.GOMAXPROCS(0)
	fmt.Println("NUMCPU:", numCPU)
	ch := make(chan error, numCPU)
	for i := 0; i < len(entities); i += 100 {
		end := i + 100
		if len(entities) < end {
			end = len(entities)
		}
		// 非同期実行
		go makeAuctownBulkUpdateFile(entities[i:end], i, ch)
	}
	for i := 0; i < len(entities); i += 100 {
		if <-ch != nil {
			log.Fatal("スレッドでエラー")
		}
	}
}

func makeAuctownBulkUpdateFile(e [][]string, i int, ch chan error) {
	if err := storeImageArchive(e, i); err != nil {
		ch <- err
	}
	if err := writeAuctownCsvFile(e, i); err != nil {
		ch <- err
	}
	ch <- nil
}

type downloadedImage struct {
	dat      []byte
	fileName string
}

func storeImageArchive(e [][]string, i int) error {
	// 画像をダウンロードする
	var images []downloadedImage
	for _, row := range e {
		log.Printf("%d 処理中: %s", i, row[0])
		imgUrls := row[6:11]
		for col, url := range imgUrls {
			if len(url) == 0 {
				continue
			}
			img, t, err := downloadImage(url)
			if err != nil {
				continue
			}
			fileName := fmt.Sprintf("%s.%s", GetMd5(url), t)
			images = append(images, downloadedImage{img, fileName})
			row[col+6] = fileName
		}
	}

	// zipにアーカイブする
	file, err := os.OpenFile(fmt.Sprintf("auctown-%05d.zip", i), os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		log.Fatalln("zipファイルオープンエラー")
	}
	defer file.Close()
	log.Println("zipファイルオープン")

	compress(images, file)

	return nil
}

// GetMd5 - get encoded password with md5
func GetMd5(password string) string {
	hash := md5.New()
	defer hash.Reset()
	hash.Write([]byte(password))
	return hex.EncodeToString(hash.Sum(nil))
}

func compress(images []downloadedImage, w *os.File) error {
	c := zip.NewWriter(w)
	defer c.Close()

	for _, img := range images {
		log.Println("zip処理中: ", img.fileName)
		f, _ := c.Create(img.fileName)
		f.Write(img.dat)
	}

	return nil
}

func downloadImage(url string) ([]byte, string, error) {

	// Download
	resp, err := http.Get(url)
	if err != nil {
		return nil, "", err
	}
	defer resp.Body.Close()

	// decode image
	img, t, err := image.Decode(resp.Body)
	if err != nil {
		return nil, t, err
	}
	log.Println("Downloaded: ", url)
	log.Println("Type of image: ", t)

	// サイズ判定
	rctSrc := img.Bounds()
	if rctSrc.Dx() < 500 && rctSrc.Dy() < 500 {
		// resize
		img = resize.Resize(500, 0, img, resize.Lanczos3)
		log.Println("画像リサイズ処理実施")
	}

	buf := new(bytes.Buffer)
	switch t {
	case "jpeg":
		if err := jpeg.Encode(buf, img, &jpeg.Options{Quality: 70}); err != nil {
			log.Fatal("JPEG Encodeエラー")
		}
	case "gif":
		if err := gif.Encode(buf, img, nil); err != nil {
			log.Fatal("GIF Encodeエラー")
		}
	case "png":
		if err := png.Encode(buf, img); err != nil {
			log.Fatal("PNG Encodeエラー")
		}
	default:
		log.Fatal("未対応Image Typeエラー")
	}
	log.Println("Encode")

	return buf.Bytes(), t, nil

}

func writeAuctownCsvFile(e [][]string, i int) error {
	// O_WRONLY:書き込みモード開く, O_CREATE:無かったらファイルを作成
	file, err := os.OpenFile(fmt.Sprintf("auctown-%05d.csv", i), os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		log.Fatalln("CSVファイルオープンエラー")
	}
	defer file.Close()
	log.Println("CSVファイルオープン: ", i)

	if err := file.Truncate(0); err != nil {
		log.Fatalln("ファイル初期化エラー")
	}

	writer := csv.NewWriter(file)
	writer.Write([]string{
		"SiteID",
		"SKU",
		"Title",
		"Category",
		"Template",
		"Description",
		"Quantity",
		"ConditionID",
		"ListingType",
		"Duration",
		"Currency",
		"StartPrice",
		"PicURL1",
		"PicURL2",
		"PicURL3",
		"PicURL4",
		"PicURL5",
		"ItemSpec",
		"UPC",
		"Brand",
	})
	for _, rows := range e {
		var spc string
		if len(rows[22]) > 0 {
			spc += fmt.Sprintf("MPN=%s;", rows[22])
		}
		if len(rows[25]) > 0 {
			spc += fmt.Sprintf("Color=%s;", rows[25])
		}
		if len(rows[26]) > 0 {
			spc += fmt.Sprintf("Size=%s;", rows[26])
		}
		if len(rows[27]) > 0 {
			spc += fmt.Sprintf("Model=%s;", rows[27])
		}
		if len(rows[28]) > 0 {
			spc += fmt.Sprintf("Material=%s;", rows[28])
		}
		for i := 30; i < 50; i += 2 {
			if len(rows[i]) == 0 {
				break
			}
			spc += fmt.Sprintf("%s=%s;", rows[i], rows[i+1])
		}
		auc := []string{
			"US",
			rows[0],
			rows[3],
			rows[20],
			"Motoki-Simple",
			rows[54],
			"1",
			"1000",
			"fixed",
			"GTC",
			"USD",
			rows[5],
			rows[6],
			rows[7],
			rows[8],
			rows[9],
			rows[10],
			spc,
			rows[23],
			rows[24],
		}
		writer.Write(auc)
	}
	writer.Flush()
	log.Println("CSVファイル書き込み: ", i)

	return nil
}

func writeXlsxFile(e [][]string) error {
	f, err := excelize.OpenFile("Kobanzame-Itemリスト.xlsx")
	if err != nil {
		log.Fatalln("エクセルのオープンエラー")
	}
	log.Println("エクセルファイルオープン")

	for row, rows := range e {
		for col, c := range rows {
			ad, _ := excelize.CoordinatesToCellName(col+1, row+2)
			_ = f.SetCellStr("ftp-get", ad, c)
		}
	}

	if err := f.Save(); err != nil {
		log.Fatalln("エクセルの保存エラー")
	}
	log.Println("エクセル保存完了")

	return nil
}

func readTsvFile(tsv string) [][]string {
	f, err := os.Open(tsv)
	if err != nil {
		log.Fatalln("TSVファイルのオープンエラー")
	}
	defer f.Close()
	log.Println("TSVファイルオープン")

	var contents [][]string
	scanner := bufio.NewScanner(f)
	i := 1
	for scanner.Scan() {
		line := scanner.Text()
		if len(strings.TrimSpace(line)) == 0 {
			continue
		}
		b := filepath.Base(tsv)
		if len(b) > 45 {
			b = b[:45]
		}
		line = b + fmt.Sprintf("-%03d", i) + "\t" + line
		ary := strings.Split(
			strings.Replace(
				line,
				",",
				" ",
				-1,
			),
			"\t",
		)
		if len(ary) != 101 {
			fmt.Println(ary)
			continue
		}
		contents = append(
			contents,
			ary,
		)
		i++
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return contents
}

func dirwalk(dir string) []string {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		panic(err)
	}

	var paths []string
	for _, file := range files {
		if file.IsDir() {
			paths = append(paths, dirwalk(filepath.Join(dir, file.Name()))...)
			continue
		}
		paths = append(paths, filepath.Join(dir, file.Name()))
	}

	return paths
}

module metabiosis

go 1.16

require (
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.4.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
)
